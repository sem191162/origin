const path = require('path')
const Course = require('../models/course')
const {Router} = require('express')

const router = Router()

router.get('/add',(req,res)=>{
    //res.sendFile(path.join(__dirname,'../views','index.html'))
    res.render('add',{title:'Добавление курса',isAdd:true})
})

router.post('/add',(req,res)=>{
    const course = new Course(req.body.title,req.body.price,req.body.img)    
    //console.log(course)
    course.save()
    
    res.redirect('/courses')
    
})
module.exports = router