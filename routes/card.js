const path = require('path')
const {Router} = require('express')
const Card = require('../models/card')
const Course = require('../models/course')

const router = Router()

router.get('/card',async (req,res)=>{
    const card = await Card.fetch()
    //console.log(card.courses)
    res.render('card',{title:'Корзина', courses: card.courses, price: card.priceSum, isCard:true})

})

router.delete('/card/remove/:id',async (req,res)=>{
    const card = await Card.remove(req.params.id)
    console.log(card)
    res.status(200).json(card)
})
router.post('/card/add', async (req,res)=>{
    //const card = new Card()
    const course = await Course.getById(req.body.id)
    //card.test()
    await Card.add(course)

    res.redirect('/courses')
})

module.exports = router