const path = require('path')
const {Router} = require('express')
const Course = require('../models/course')
const router = Router()


router.get('/courses', async (req,res)=>{
    //res.sendFile(path.join(__dirname,'../views','index.html'))
    const courses = await Course.getAll()
    
    res.render('courses',{title:'Курсы',isCourses:true,courses})
})

 router.get('/courses/:id', async (req,res)=>{
     const course = await Course.getById(req.params.id)
   
     res.render('course',{course,title:`Курс `})
})

router.get('/courses/:id/edit', async (req,res)=>{
    const course = await Course.getById(req.params.id)
  
    res.render('courseEdit',{course,title:`Редактор курса `})
})

router.post('/courses/update/', async (req,res)=>{
    
    let course = new Course(req.body.title,req.body.price,req.body.img)    
    course.id = req.body.id

   // console.log(course)

    await course.update(course)
  
    res.redirect('/courses')
})


module.exports = router