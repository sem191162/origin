
function remove_course(id,ob){
    const price = Number(ob.parentNode.parentNode.cells[1].innerText)
    var obKol   = ob.parentNode.parentNode.cells[2]
    var kol = Number(obKol.innerText) 
    var obPrice = document.querySelector('#span_price')
    var summa = Number(obPrice.innerText)
    summa -= price
    fetch('/card/remove/'+id,{
        method:'delete'
    }).then(res => res.json()).then(card => {
        //console.log(card)
        if(kol===1){
            ob.parentNode.parentNode.remove()
        } else {
          obKol.innerText = --kol
        }
        obPrice.innerText = summa
    })
}