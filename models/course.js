const uuid = require('uuid/v4')
const path = require('path')
const fs = require('fs')

class Course{
    constructor(title,price,img){
        this.title = title
        this.price = price
        this.img = img
        this.id = uuid()
        this.path = path.join(__dirname,'..','data','courses.json')
    }

    toJSON(){
        return {
            id : this.id,
            title : this.title,
            price : this.price,
            img : this.img
            
        }
    }

    async update(curs){
        
        const courses = await Course.getAll()

        console.log(curs)
        
        const idx = courses.findIndex(c => c.id === curs.id)
        
        courses[idx] = curs

         return new Promise((resolve,reject)=>{ 
            fs.writeFile(path.join(__dirname,'..','data','courses.json'),
            JSON.stringify(courses),
            (err)=>{
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            })
        
         })
    }
    async save(){
        //console.log(this.toJSON())
        const courses = await Course.getAll()
        courses.push(this.toJSON())

        return new Promise((resolve,reject)=>{ 
            fs.writeFile(path.join(__dirname,'..','data','courses.json'),
            JSON.stringify(courses),
            (err)=>{
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            })
        })
   }
   
    static async getById(id){
        const courses = await Course.getAll()
        return courses.find(c => c.id === id)
    }

    static getAll(){
        return new Promise((resolve,reject)=>{
            //console.log(this.getPath())
            fs.readFile(
                path.join(__dirname,'..','data','courses.json'),
                'utf-8',
                (err,content) => {
                    if (err) {
                        reject(err)
                    } else {
                        // console.log(content)
                        //let tt = JSON.parse(content)
                        //console.log(tt)
                        resolve(JSON.parse(content))
                    }
    
                }
            ) 
        })
       
        //return  JSON.parse(content)
    }    

   getPath(){
       return path.join(__dirname,'..','data','courses.json') 
   } 
}

module.exports = Course