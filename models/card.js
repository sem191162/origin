const uuid = require('uuid/v4')
const path = require('path')
const fs = require('fs')


class Card {
    static async add(course){
      // console.log(course)
       const card = await this.fetch()
              
    //    return new Promise((resolve,reject)=>{
    //       resolve()
    //    })
       if(card.courses.length){
            const idx = card.courses.findIndex(c => c.id === course.id)
           if (idx >= 0) {
            card.courses[idx].count++    
           } else {
            course.count = 1
            card.courses.push(course)    
           }
       } else {
           course.count = 1
           card.courses.push(course)    
        }
    //    course.count = 1
    //    card.courses.push(course)
       card.priceSum += +course.price
       //console.log(card)
       return new Promise((resolve,reject)=>{ 
        fs.writeFile(path.join(__dirname,'..','data','card.json'),
        JSON.stringify(card),
        (err)=>{
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            })
       })
    
    }
    static async remove(id){
        const card = await Card.fetch()

        const idx = card.courses.findIndex(c => c.id === id)
        const course = card.courses[idx] 

        if (course.count === 1) {
           card.courses = card.courses.filter(c => c.id !== id) 
        } else {
            card.courses[idx].count-- 
        }
        card.priceSum -= course.price
        return new Promise((resolve,reject)=>{ 
            fs.writeFile(path.join(__dirname,'..','data','card.json'),
            JSON.stringify(card),
            (err)=>{
                    if (err) {
                        reject(err)
                    } else {
                        resolve(card)
                    }
                })
           })

    } 
    static async fetch(){
        return new Promise((resolve,reject)=>{
            //console.log(this.getPath())
            fs.readFile(
                path.join(__dirname,'..','data','card.json'),
                'utf-8',
                (err,content) => {
                    if (err) {
                        reject(err)
                    } else {
                        // console.log(content)
                        //let tt = JSON.parse(content)
                        //console.log(tt)
                        resolve(JSON.parse(content))
                    }
    
                }
            )}
        )    
    }
    test(){
        console.log("Card runnig")
    }
}

module.exports = Card